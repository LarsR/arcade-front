package main

import (
	"log"
	_ "strconv"
	"sync"
	"time"

	"github.com/mxk/go-sqlite/sqlite3"
)

// Database ,struct for handling databtionsase ac
type Database struct {
	file  string
	conn  *sqlite3.Conn
	inuse *sync.Mutex
}

// Close unlock
func (db *Database) Close() {
	db.inuse.Unlock()
}

// Setup Create table items
func (db *Database) Setup(file string) {
	db.inuse = &sync.Mutex{}
	db.inuse.Lock()
	defer db.inuse.Unlock()
	d, err := PathToABS(file)
	LogFatal(err)
	db.file = d
	db.conn, err = sqlite3.Open(d)
	LogFatal(err)
	err = db.conn.Exec(`CREATE TABLE IF NOT EXISTS items (name UNIQUE,
                                                     title TEXT,
                                                     image TEXT,
                                                     type TEXT,
                                                     last TEXT,
                                                     count INTEGER,
                                                     enabled INTEGER)`)
	LogFatal(err)
}

// DropGamesByEmulator Remove all games for one emulator
func (db *Database) DropGamesByEmulator(emuName string) {
	db.inuse.Lock()
	defer db.inuse.Unlock()
	err := db.conn.Exec("DELETE FROM items WHERE type='" + emuName + "'")
	LogError(err)
}

// InsertGame Add a game to table
func (db *Database) InsertGame(name string, title string, image string, gameType string, enabled int) {
	db.inuse.Lock()
	defer db.inuse.Unlock()

	args := sqlite3.NamedArgs{"$name": name, "$title": title, "$image": image, "$type": gameType, "$enabled": enabled}
	err := db.conn.Exec("INSERT INTO items VALUES($name,$title,$image,$type,'',0,$enabled)", args)
	LogError(err)
}

// UpdateExec ,update count and last for selected game
func (db *Database) UpdateExec(name string) {
	db.inuse.Lock()
	defer db.inuse.Unlock()
	d := time.Now()
	err := db.conn.Exec("UPDATE items SET last='" + d.String() + "', count = count + 1 WHERE name='" + name + "'")
	LogError(err)
}

// Enable ,update enable field with value
func (db *Database) Enable(name string, value bool) {
	db.inuse.Lock()
	defer db.inuse.Unlock()

	enable := "0"
	if value {
		enable = "1"
	}
	err := db.conn.Exec("UPDATE items SET enabled='" + enable + "' WHERE name='" + name + "'")
	LogError(err)
}

// Enable ,update enable field with value
func (db *Database) Update(name string, enabled bool, title string) {
	db.inuse.Lock()
	defer db.inuse.Unlock()

	enable := "0"
	if enabled {
		enable = "1"
	}
	log.Println(name, title)
	log.Println("UPDATE items SET enabled = '" + enable + "', title = '" + title + "' WHERE name='" + name + ".zip'")
	err := db.conn.Exec("UPDATE items SET enabled = '" + enable + "', title = '" + title + "' WHERE name='" + name + ".zip'")
	LogError(err)
}

// SetImage set image for selected name
func (db *Database) SetImage(name string, image string) {
	db.inuse.Lock()
	defer db.inuse.Unlock()
	err := db.conn.Exec("UPDATE items SET image='" + image + "' WHERE name='" + name + "'")
	LogError(err)
}

// Games receive all games from items ordered by count
func (db *Database) Games() []*Game {
	db.inuse.Lock()
	defer db.inuse.Unlock()

	i := int32(0)
	var games []*Game
	query := "SELECT * FROM items WHERE enabled=1 ORDER BY count DESC"
	s, err := db.conn.Query(query)
	if LogWarning(err) {
		return games
	}
	for err == nil {
		var game Game
		game.Index = i
		s.Scan(&game.Name, &game.Title, &game.Image, &game.Type, &game.Last, &game.Count, &game.Enabled)
		games = append(games, &game)
		i++
		err = s.Next()
	}
	s.Close()
	return games
}

// Games receive all games from items ordered by count
func (db *Database) AllGames() []*Game {
	db.inuse.Lock()
	defer db.inuse.Unlock()

	i := int32(0)
	var games []*Game
	query := "SELECT * FROM items ORDER BY count DESC"
	s, err := db.conn.Query(query)
	if LogWarning(err) {
		return games
	}
	for err == nil {
		var game Game
		game.Index = i
		s.Scan(&game.Name, &game.Title, &game.Image, &game.Type, &game.Last, &game.Count, &game.Enabled)
		games = append(games, &game)
		i++
		err = s.Next()
	}
	s.Close()
	return games
}

// Game ,get single game form items
func (db *Database) Game(name string) Game {
	db.inuse.Lock()
	defer db.inuse.Unlock()
	rows, err := db.conn.Query("SELECT * FROM items WHERE name='" + name + "' LIMIT 1")
	game := Game{}
	if err == nil {
		rows.Scan(&game.Name, &game.Title, &game.Image, &game.Type, &game.Last, &game.Count, &game.Enabled)
		return game
	}
	rows.Close()
	return game
}
