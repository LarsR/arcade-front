package main

import (
	"flag"
	"fmt"
	"runtime"
)

var (
	app   Application
	debug bool
)

func main() {
	var config Configuration
	var db Database
	runtime.LockOSThread()
	importgames := flag.String("import-games", "", "Import all games for selected emulator.")
	updatemame := flag.Bool("update-mame", false, "Update title and enabled from mame executable")

	dropgames := flag.String("drop-games", "", "Remove all games from database for selected emulator")
	conf := flag.String("config", "assets/config.toml", "Select config to load")
	flag.BoolVar(&debug, "debug", false, "output to stdout")
	flag.Parse()
	LogSetup()

	f, err := PathToABS(*conf)
	LogFatal(err)

	err = config.loadFromFile(f)
	LogFatal(err)

	db.Setup(config.Database)

	if *importgames != "" {
		fmt.Println("Import games for emulator: " + *importgames)
		ImportGames(config, db, *importgames)
		return
	}

	if *updatemame {
		fmt.Println("Update mame games")
		UpdateMame(config, db)
		return
	}

	if *dropgames != "" {
		fmt.Println("Drop all games for emulator: " + *dropgames)
		db.DropGamesByEmulator(*dropgames)
		return
	}

	go NewWebApi(config, &db)

	app.Start(&config, &db)

	app.Destroy()
	LogDefer()
}
