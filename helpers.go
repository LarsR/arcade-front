package main

import (
	"os"
	"path/filepath"
	"strings"

	"github.com/veandco/go-sdl2/sdl"
)

// PathToABS Return absolute path
func PathToABS(inPath string) (string, error) {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		return inPath, err
	}
	if !strings.HasPrefix(inPath, "/") {
		inPath = dir + "/" + inPath
	}
	return inPath, nil
}

// RectangleFromList Return a sdl.Rect from a list of int32
func RectangleFromList(points [4]int32) sdl.Rect {
	return sdl.Rect{points[0], points[1], points[2], points[3]}
}

// InRange return if value is between from and to
func InRange(value, from, to int32) bool {
	return (value > from && value < to)
}

// Min return smallest int32
func Min(a, b int32) int32 {
	if a < b {
		return a
	}
	return b
}

// Max return biggest int32
func Max(a, b int32) int32 {
	if a > b {
		return a
	}
	return b
}
