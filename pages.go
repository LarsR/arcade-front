package main

import (
	"github.com/veandco/go-sdl2/sdl"

	"math"
	"strconv"
)

// Pages contains a collection of page and also keep current PageNumber
type Pages struct {
	app          *Application
	Items        []*Page
	paging       *Paging
	CurrentPage  *int32
	Totalpages   *int32
	pageInfoRect *sdl.Rect
	Selected     *Size
	Layout       *Size
	itemSize     *Size
	page         *sdl.Texture
	step         *int32
}

func (pages *Pages) init(app *Application) {
	pages.app = app
	games := app.db.Games()
	pages.CurrentPage = new(int32)
	pages.Totalpages = new(int32)
	pages.step = new(int32)
	pages.Selected = new(Size)
	pages.Selected.H = 1
	pages.Selected.W = 1
	*pages.step = 1
	pages.Layout = app.config.Layout
	*pages.CurrentPage = 1
	numPerPages := int32(app.config.Layout.W * app.config.Layout.H)
	start := int32(0)
	p := int32(0)
	w, h := pages.app.window.GetSize()
	pages.itemSize = new(Size)
	pages.itemSize.W = (int32(w) - (pages.app.config.Margin * (pages.Layout.W + 1))) / pages.Layout.W
	pages.itemSize.H = (int32(h) - (pages.app.config.Margin * (pages.Layout.H + 1))) / pages.Layout.H
	for start < int32(len(games)) {
		p++
		page := new(Page)
		end := int32(math.Min(float64(start+numPerPages), float64(len(games))))
		page.init(games[start:end], pages, p)
		pages.Items = append(pages.Items, page)
		start = start + numPerPages
	}
	*pages.Totalpages = p
	pages.paging = new(Paging)
	pages.paging.init(pages)
}

func (pages *Pages) renderSelection() {
	margin := pages.app.config.Margin
	width := pages.itemSize.W
	height := pages.itemSize.H
	top := ((pages.Selected.H - 1) * (height + margin)) + margin
	left := ((pages.Selected.W - 1) * (width + margin)) + margin

	dst := []sdl.Rect{
		{left - 15, top - 15, width + 30, 15},
		{left + width, top, 15, height},
		{left - 15, top + height - 15, width + 30, 30},
		{left - 15, top, 15, height},
	}
	pages.app.renderer.SetDrawColor(208, 208, 25, 192)
	pages.app.renderer.FillRects(dst)

	page := pages.Items[*pages.CurrentPage-1]
	num := ((pages.Selected.H - 1) * pages.Layout.W) + (pages.Selected.W - 1)
	if int(num) >= len(page.games) {
		return
	}
	game := page.games[num]
	surface, err := pages.app.fonts.Font16.RenderUTF8Solid(game.Title, pages.app.config.GameTitle.Color)
	defer surface.Free()
	if LogWarning(err) {
		return
	}
	pageInfo, err := pages.app.renderer.CreateTextureFromSurface(surface)
	if LogWarning(err) {
		return
	}
	defer pageInfo.Destroy()

	textw, texth, err := pages.app.fonts.Font16.SizeUTF8(game.Title)
	if LogWarning(err) {
		return
	}
	// r := sdl.Rect{x,y,w,h}
	r := sdl.Rect{0, 0, int32(textw), int32(texth)}
	tTop := (top + height - 15) + ((30 - int32(texth)) / 2)
	tLeft := left + ((width - int32(textw)) / 2)
	t := sdl.Rect{tLeft, tTop, int32(textw), int32(texth)}
	pages.app.renderer.Copy(pageInfo, &r, &t)

}

func (pages *Pages) renderPageInfo() {
	str := "Page: " + strconv.Itoa(int(*pages.CurrentPage)) + " / " + strconv.Itoa(int(*pages.Totalpages))
	surface, err := pages.app.fonts.Font16.RenderUTF8Solid(str, pages.app.config.PageInfo.Color)
	defer surface.Free()
	if LogWarning(err) {
		return
	}
	pageInfo, err := pages.app.renderer.CreateTextureFromSurface(surface)
	if LogWarning(err) {
		return
	}
	defer pageInfo.Destroy()
	w, h, err := pages.app.fonts.Font16.SizeUTF8(str)
	if LogWarning(err) {
		return
	}
	wi, _ := pages.app.window.GetSize()
	pages.pageInfoRect = &sdl.Rect{0, 0, int32(w), int32(h)}

	r := sdl.Rect{int32(wi) - (pages.pageInfoRect.W + 40), int32(10), pages.pageInfoRect.W, pages.pageInfoRect.H}

	pages.app.renderer.Copy(pageInfo, pages.pageInfoRect, &r)
}

func (pages *Pages) update() bool {
	var (
		err     error
		changed bool
	)
	if pages.handleInput() {
		changed = true
	}
	for _, page := range pages.Items {
		if page.update() {
			changed = true
		}
	}

	if pages.page != nil {
		pages.page.Destroy()
	}
	if changed && pages.Items[*pages.CurrentPage-1].surface != nil {
		pages.page, err = pages.app.renderer.CreateTextureFromSurface(pages.Items[*pages.CurrentPage-1].surface)
		LogError(err)
		w, h := pages.app.window.GetSize()
		src := sdl.Rect{0, 0, int32(w), int32(h)}
		err = pages.app.renderer.Copy(pages.page, &src, &src)
		LogError(err)
		pages.renderSelection()
		pages.renderPageInfo()
	}
	pages.paging.update()
	return changed
}

func (pages *Pages) handleInput() bool {
	changed := false
	if pages.app.input.Up.Pressed() {
		if *pages.paging.active {
			if pages.previousPage() {
				changed = true
			}
		} else {
			if pages.Selected.H > 1 {
				pages.Selected.H--
				changed = true
			} else {
				if pages.previousPage() {
					changed = true
					pages.Selected.H = pages.Layout.H
				}
			}
		}
	}

	if pages.app.input.Down.Pressed() {
		if *pages.paging.active {
			if pages.nextPage() {
				changed = true
			}
		} else {

			if pages.Selected.H < pages.Layout.H {
				pages.Selected.H++
				if !pages.has() {
					pages.Selected.H--
				}
				changed = true
			} else {
				if pages.nextPage() {
					pages.Selected.H = 1
					changed = true
				}

			}
		}
	}

	if pages.app.input.Next.Pressed() {
		if pages.nextPage() {
			changed = true
		}

	}

	if pages.app.input.Previous.Pressed() {
		if *pages.CurrentPage > 1 {
			*pages.CurrentPage--
			changed = true
		}

	}

	if pages.app.input.Left.Pressed() {
		if pages.Selected.W > 1 && !*pages.paging.active {
			pages.Selected.W--
			changed = true
		}
		if *pages.paging.active == true {
			*pages.paging.active = false
			changed = true
		}
	}
	if pages.app.input.Right.Pressed() {
		if pages.Selected.W < pages.Layout.W {
			pages.Selected.W++
			if !pages.has() {
				pages.Selected.W--
			}

			changed = true
		} else {
			//*pages.paging.active = true
			//changed = true
		}
	}

	if pages.app.input.Select.Pressed() {
		page := pages.Items[*pages.CurrentPage-1]
		num := ((pages.Selected.H - 1) * pages.Layout.W) + (pages.Selected.W - 1)
		game := page.games[num]
		sdl.PumpEvents()
		sdl.FlushEvent(sdl.KEYDOWN)
		sdl.FlushEvent(sdl.PRESSED)
		pages.app.Exec(game)
		changed = true

	}
	return changed
}

func (pages *Pages) previousPage() bool {
	if *pages.CurrentPage > 1 {
		*pages.CurrentPage--
		return true
	}
	return false
}

func (pages *Pages) nextPage() bool {
	if *pages.CurrentPage < *pages.Totalpages {
		*pages.CurrentPage++
		if !pages.has() {
			*pages.CurrentPage--
			return false
		}
		return true
	}
	return false
}

func (pages *Pages) has() bool {
	if int32(len(pages.Items)) < *pages.CurrentPage {
		return false
	}
	page := pages.Items[*pages.CurrentPage-1]
	num := ((pages.Selected.H - 1) * pages.Selected.W) + (pages.Selected.W - 1)
	if int32(len(page.games)) < num {
		return false
	}
	return true
}
