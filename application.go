package main

import (
	"os/exec"
	"strings"
	"time"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
)

// Application sdl gui application
type Application struct {
	config   *Configuration
	window   *sdl.Window
	renderer *sdl.Renderer
	input    Input
	focused  *string
	pages    *Pages
	settings *Settings
	surface  *sdl.Surface
	fonts    Fonts
	execute  chan *Game
	db       *Database
	running  *bool
}

// Start run application
func (a *Application) Start(c *Configuration, db *Database) {
	a.config = c
	a.running = new(bool)
	a.focused = new(string)
	a.db = db
	a.Setup()
	a.input = NewInput(c)
	a.pages = new(Pages)
	a.pages.init(a)
	a.run()
}

// Setup initialize window renderer and base textures
func (a *Application) Setup() {
	sdl.Init(sdl.INIT_EVERYTHING)
	img.Init(img.INIT_JPG | img.INIT_PNG)
	ttf.Init()
	a.fonts.init()
	LogFatal(a.initWindow())
	LogFatal(a.initRenderer())
	LogFatal(a.loadSurface())
}

func (a *Application) initWindow() error {
	var err error
	var flags uint32 = sdl.WINDOW_FULLSCREEN
	if !a.config.Fullscreen {
		flags = sdl.WINDOW_RESIZABLE | sdl.WINDOW_SHOWN
	}

	w, h := a.getSize()
	a.window, err = sdl.CreateWindow(a.config.Title, sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED, int32(w), int32(h), flags)
	return err
}

func (a *Application) initRenderer() error {
	var err error
	a.renderer, err = sdl.CreateRenderer(a.window, -1, sdl.RENDERER_ACCELERATED)
	return err
}

func (a *Application) loadSurface() error {
	path, err := PathToABS(a.config.Src)
	if err != nil {
		return err
	}
	a.surface, err = img.Load(path)

	if err != nil {
		return err
	}
	return nil
}

func (a *Application) run() bool {
	*a.running = true
	for *a.running {
		if a.input.Close() || a.input.Exit.Pressed() {
			a.Destroy()
			return false
		}
		if a.input.Reload.Pressed() {
			a.Destroy()
			return true
		}
		if a.pages.update() {
			a.renderer.Present()
			a.renderer.Clear()
		}
		sdl.PumpEvents()
		sdl.Delay(10)
	}
	return true
}
func (a *Application) getSize() (int32, int32) {
	w := a.config.WindowSize.W
	h := a.config.WindowSize.H
	return w, h
}

// Destroy window renderer and texture also quit sdl and ttf
func (a *Application) Destroy() {
	a.renderer.Destroy()
	a.window.Destroy()
	ttf.Quit()
	sdl.Quit()
}

// Exec will execute game from input game
func (a *Application) Exec(game *Game) {
	cmd := a.command(game)
	if cmd == nil {
		return
	}
	LogMsg("Execute game: " + game.Title)
	a.db.UpdateExec(game.Name)
	a.Destroy()
	err := cmd.Start()
	LogInfo(err)
	cmd.Wait()
	time.Sleep(1 * time.Second)
	a.Setup()
}

func (a *Application) command(game *Game) *exec.Cmd {
	emulator := a.config.emulator(game.Type)
	if emulator == nil {
		LogMsg("Missing Emulator")
		return nil
	}
	attr := emulator.Attributes
	fl := game.Name
	if emulator.SkipExtension {
		fl = strings.TrimSuffix(fl, emulator.RomExtension)
	}
	attr = append(attr, fl)
	return exec.Command(emulator.Command, attr...)
}
