package main

import (
	"errors"
	"math"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
)

// Page contains a list of games and
type Page struct {
	games      []*Game
	num        int32
	rect       *Rect
	surface    *sdl.Surface
	pages      *Pages
	app        *Application
	preloading *int32
}

func (page *Page) init(games []*Game, pages *Pages, num int32) error {
	page.games = games
	page.app = pages.app
	page.pages = pages
	page.num = num

	return nil
}
func (page *Page) shouldPreload() bool {
	if (page.surface == nil || page.preloading != nil) &&
		page.num == *page.pages.CurrentPage {
		return true
	}
	return false
}

func (page *Page) shouldDispose() bool {
	if page.surface != nil &&
		page.num != *page.pages.CurrentPage {
		return true
	}

	return false
}

func (page *Page) update() bool {
	changed := false
	if page.shouldPreload() {
		if page.preloading == nil {
			page.preloading = new(int32)
		}
		err := page.blitSurface()
		LogWarning(err)
		*page.preloading++

		if *page.preloading >= int32(len(page.games)) {
			page.preloading = nil
		}
		changed = true
	}

	if page.shouldDispose() {
		page.surface.Free()
		page.surface = nil
		page.preloading = nil
		return false
	}
	return changed
}

func (page *Page) blitSurface() error {
	var (
		err   error
		index int32
	)
	index = *page.preloading
	if index >= int32(len(page.games)) {
		return errors.New("index out of range")
	}
	w, h := page.app.window.GetSize()

	game := page.games[index]

	if page.surface == nil {
		page.surface, err = sdl.CreateRGBSurface(0, int32(w), int32(h), 32, 0, 0, 0, 0)
		if err != nil {
			return err
		}
	}

	path := game.findImage(page.app, ".png")
	if path == "" {
		path = game.findImage(page.app, ".jpg")
	}

	if path == "" {
		err = page.merge(page.app.surface, page.app.config.FanartFallback.Rectangle)
		LogError(err)
		page.addTitle(game.Title)
		return errors.New("can not find any image using default")
	}

	surface, err := img.Load(path)
	defer surface.Free()
	if LogError(err) {
		err = page.merge(page.app.surface, page.app.config.FanartFallback.Rectangle)
		if len(game.Title) > 16 {

			page.addTitle(game.Title[:16])
		} else {
			page.addTitle(game.Title)

		}
		return err
	}

	err = page.merge(surface, sdl.Rect{0, 0, surface.W, surface.H})
	page.addTitle(game.Title)
	return err
}

func (page *Page) addTitle(title string) {
	row := int32(math.Ceil(float64(*page.preloading / page.pages.Layout.W)))
	col := *page.preloading - (row * page.pages.Layout.W)
	left := int32(((page.pages.itemSize.W + page.app.config.Margin) * col) + page.app.config.Margin)
	top := int32((page.pages.itemSize.H+page.app.config.Margin)*row + page.app.config.Margin + (page.pages.itemSize.H - 15))
	dst := sdl.Rect{left, top, page.pages.itemSize.W, 30}
	src := sdl.Rect{0, 0, page.pages.itemSize.W, 30}
	surface, err := page.app.fonts.Font16.RenderUTF8Solid(title, page.app.config.GameTitle.Color)
	defer surface.Free()
	if LogWarning(err) {
		return
	}

	textw, texth, err := page.app.fonts.Font16.SizeUTF8(title)
	if LogWarning(err) {
		return
	}

	sf2, err := sdl.CreateRGBSurface(0, page.pages.itemSize.W, page.pages.itemSize.H, 32, 0, 0, 0, 0)
	sf2.FillRect(nil, sdl.MapRGB(sf2.Format, 218, 218, 218))
	defer sf2.Free()
	l := (page.pages.itemSize.W - int32(textw)) / 2
	t := (30 - int32(texth)) / 2
	err = surface.BlitScaled(&src, sf2, &sdl.Rect{l, t, page.pages.itemSize.W, 30})
	if LogError(err) {
		return
	}
	err = sf2.BlitScaled(&src, page.surface, &dst)
	LogError(err)
}

func (page *Page) merge(surface *sdl.Surface, src sdl.Rect) error {
	row := int32(math.Ceil(float64(*page.preloading / page.pages.Layout.W)))
	col := *page.preloading - (row * page.pages.Layout.W)
	left := int32(((page.pages.itemSize.W + page.app.config.Margin) * col) + page.app.config.Margin)
	top := int32((page.pages.itemSize.H+page.app.config.Margin)*row + page.app.config.Margin)
	dst := sdl.Rect{left, top, page.pages.itemSize.W, page.pages.itemSize.H}
	err := surface.BlitScaled(&src, page.surface, &dst)
	return err
}
