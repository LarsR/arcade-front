package main

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"strings"
)

// NewWebApi function to create a new webapi
func NewWebApi(c Configuration, db *Database) *WebAPI {
	var api WebAPI
	api.config = c
	api.db = db

	http.HandleFunc("/enable", api.enable)
	http.HandleFunc("/execute", api.exe)
	http.HandleFunc("/set_image", api.setImage)
	http.HandleFunc("/", api.handleRequest)

	http.ListenAndServe(":"+strconv.Itoa(c.HTTPPort), nil)
	return &api
}

// WebAPI Struct of WebApi
type WebAPI struct {
	config Configuration
	db     *Database
}

func (api *WebAPI) enable(w http.ResponseWriter, r *http.Request) {
	name := r.FormValue("name")
	if name == "" || r.FormValue("value") == "" {
		http.Error(w, "Missing page or title data", http.StatusNotFound)
		return
	}
	value := (r.FormValue("value") == "true")
	api.db.Enable(name, value)
}

func (api *WebAPI) setImage(w http.ResponseWriter, r *http.Request) {
	name := r.FormValue("name")
	image := r.FormValue("image")
	if name == "" {
		http.Error(w, "Missing name data", http.StatusNotFound)
		return
	}
	api.db.SetImage(name, image)
}

func (api *WebAPI) exe(w http.ResponseWriter, r *http.Request) {
	name := r.FormValue("name")

	if name == "" {
		http.Error(w, "Missing name", http.StatusNotFound)
		return
	}
	game := api.db.Game(name)
	fmt.Println(game.Name)
}

func (api *WebAPI) handleRequest(w http.ResponseWriter, r *http.Request) {

	if r.RequestURI == "/" {
		p, err := PathToABS("assets/html/page.html")
		if api.NotFound(err, w, r) {
			return
		}
		t, err := template.New("page.html").ParseFiles(p)
		if api.NotFound(err, w, r) {
			return
		}

		data := api.buildData()

		err = t.Execute(w, data)
		api.NotFound(err, w, r)
	} else {
		p, err := PathToABS(strings.TrimLeft(r.RequestURI, "/"))
		if api.NotFound(err, w, r) {
			return
		}
		http.ServeFile(w, r, p)
	}
}

func (api *WebAPI) buildData() ResponseData {
	data := ResponseData{}
	data.Config = api.config
	data.Games = api.db.AllGames()
	return data
}

func (api *WebAPI) NotFound(err error, w http.ResponseWriter, r *http.Request) bool {
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		LogWarning(err)
		return true
	}
	return false
}

type ResponseData struct {
	Config Configuration
	Games  []*Game
}
