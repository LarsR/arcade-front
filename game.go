package main

import (
	"os"
	"strings"
)

// Game contains information of each game in database
type Game struct {
	Name    string
	Title   string
	Image   string
	Type    string
	Last    string
	Count   int
	Enabled bool
	Index   int32
}

func (g Game) findImage(app *Application, extension string) string {
	filename := strings.TrimSuffix(g.Name, ".zip") + extension

	path, err := PathToABS("assets/fanarts/" + filename)
	if err == nil {
		_, err = os.Stat(path)
		if err == nil {
			return path
		}
	}
	emu := app.config.emulator(g.Type)
	path = emu.ScreenshotPath + "/" + filename
	_, err = os.Stat(path)
	if err == nil {
		return path
	}
	return ""
}
