package main

import "github.com/veandco/go-sdl2/sdl"
import "strconv"
import "math"

// Paging Container for page control
type Paging struct {
	active      *bool
	pages       *Pages
	app         *Application
	numbersSize *Size
}

func (paging *Paging) init(pages *Pages) {
	paging.active = new(bool)
	paging.pages = pages
	paging.app = pages.app
	paging.setupTexture()

}
func (paging *Paging) setupTexture() {
	last := strconv.Itoa(int(*paging.pages.Totalpages))
	w, h, err := paging.app.fonts.Font16.SizeUTF8(last)
	if LogError(err) {
		return
	}
	paging.numbersSize = new(Size)
	paging.numbersSize.W = int32(w)
	paging.numbersSize.H = int32(h)

}

func (paging *Paging) update() {
	if !*paging.active {
		return
	}
	w, h := paging.app.window.GetSize()
	paging.app.renderer.SetDrawColor(208, 208, 25, 192)
	paging.app.renderer.FillRect(&sdl.Rect{int32(w) - paging.numbersSize.W, 0, paging.numbersSize.W, int32(h)})
	actual := *paging.pages.CurrentPage
	items := math.Ceil(float64(h) / float64(paging.numbersSize.H))
	start := actual - (int32(items) / 2)
	end := actual + (int32(items) / 2)
	y := int32(10)

	for current := start; current <= end; current++ {
		if current > *paging.pages.Totalpages {
			return
		}
		if current < 1 {
			y += paging.numbersSize.H
			continue
		}
		surface, err := paging.app.fonts.Font16.RenderUTF8Solid(strconv.Itoa(int(current)), paging.app.config.GameTitle.Color)
		if current == actual {
			surface, err = paging.app.fonts.Font20.RenderUTF8Solid(strconv.Itoa(int(current)), paging.app.config.GameTitle.Color)

		}
		defer surface.Free()
		if LogWarning(err) {
			y += paging.numbersSize.H
			continue
		}
		texture, err := paging.app.renderer.CreateTextureFromSurface(surface)

		if LogWarning(err) {
			y += paging.numbersSize.H
			continue
		}
		src := sdl.Rect{0, 0, surface.W, surface.H}
		dst := sdl.Rect{int32(w) - paging.numbersSize.W, y, surface.W, surface.H}

		if current == actual {
			paging.app.renderer.SetDrawColor(208, 25, 25, 192)
			paging.app.renderer.FillRect(&sdl.Rect{int32(w) - paging.numbersSize.W - 15, y - 3, paging.numbersSize.W + 15, paging.numbersSize.H + 6})
		}
		paging.app.renderer.Copy(texture, &src, &dst)
		y += paging.numbersSize.H

	}

}
