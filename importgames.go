package main

import (
	"log"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"
)

// SyncGames , will add missing games to items
func SyncGames(c Configuration, db Database) {
	for _, emu := range c.Emulators {
		if emu.RomPath != "" {
			path, err := PathToABS(emu.RomPath)
			if LogError(err) {
				return
			}

			path = strings.TrimSuffix(path, "/")
			path = path + "/*" + emu.RomExtension
			files, err := filepath.Glob(path)
			if LogError(err) {
				return
			}
			for _, file := range files {
				name := filepath.Base(file)
				title := filepath.Base(file)
				title = strings.TrimSuffix(title, emu.RomExtension)

				n1 := strings.ToUpper(title[0:1])
				n2 := title[1:]
				title = n1 + n2
				db.InsertGame(name, title, "", emu.Name, 0)
			}
		}

	}
}

//UpdateMame Update mamev table
func UpdateMame(c Configuration, db Database) {
	emu := c.emulator("mame")

	if emu == nil {
		log.Println("Can not find emulator for mame")
		return
	}
	out, err := exec.Command(emu.Command, "-listinfo").Output()
	if err != nil {
		log.Fatal(err)
	}
	log.Println(string(out))
	s := strings.Split(string(out), "game (\n")
	for _, g := range s {
		name := ""
		title := ""
		re := regexp.MustCompile("\tname (.*)")
		desc := regexp.MustCompile("\tdescription \"(.*)\"")

		enabled := !strings.Contains(g, "cloneof")

		f := re.FindStringSubmatch(g)
		if len(f) > 0 {
			name = f[1]
		}

		d := desc.FindStringSubmatch(g)
		if len(d) > 0 {
			title = d[1]
		}

		db.Update(name, enabled, title)
	}

}

// ImportGames ,will import all games based on name
func ImportGames(c Configuration, db Database, name string) {
	emu := c.emulator(name)
	if emu != nil {
		if emu.RomPath != "" {
			path, err := PathToABS(emu.RomPath)
			if LogError(err) {
				return
			}
			path = strings.TrimSuffix(path, "/")
			path = path + "/*" + emu.RomExtension
			files, err := filepath.Glob(path)
			if LogError(err) {
				return
			}
			for _, file := range files {
				name := filepath.Base(file)
				title := filepath.Base(file)
				title = strings.TrimSuffix(title, emu.RomExtension)

				n1 := strings.ToUpper(title[0:1])
				n2 := title[1:]
				title = n1 + n2
				db.InsertGame(name, title, "", emu.Name, 0)
			}
		}
	}
}
